import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class main {
	public static void main(String[] args) throws IOException, InterruptedException, ParseException {
		if(Automatic()) {
			SendEmailNotification();
			System.out.println("Run Success");
		} else {
			System.out.println("Run Fail");
		}
	}
	public static boolean Automatic() throws IOException, InterruptedException, ParseException {
		System.setProperty("file.encoding","UTF-8");
		ChromeOptions ops = new ChromeOptions();
		ops.addArguments("--disable-notifications");
		System.setProperty("webdriver.chrome.driver", "chromedriver_win32.exe");
		WebDriver driver = new ChromeDriver(ops);
		driver.manage().window().maximize();
		driver.get("https://phone.phanmemninja.com/home");
		String login_email = "procurement@reputable.asia";
		String login_password = "0901486137"; //	procurement@reputable.asia - 0901486137
		WebElement email_input = driver.findElement(By.name("email"));
		email_input.sendKeys(login_email);
		WebElement password_input = driver.findElement(By.name("password"));
		password_input.sendKeys(login_password);
		WebElement login_button = driver.findElement(By.cssSelector("form.login-form button"));
		login_button.click();
		WebElement checkbox_button = driver.findElement(By.cssSelector("input.form-check-input-styled"));
		if(!checkbox_button.isSelected()) {
			checkbox_button.click();
		}
		WebElement utility_button = driver.findElement(By.xpath("//*[contains(text(), 'Tiện ích')]"));
		utility_button.click();
		WebElement setup_button = driver.findElement(By.xpath("//*[contains(text(), 'Cấu hình tương tác')]"));
		setup_button.click();
		Select dropdown = new Select(driver.findElement(By.cssSelector("div#modalConfig select")));
		 dropdown.selectByValue("7787"); 
		 // value of kịch bản 01 = 6328
		 // value of tương tác = 6386
		 // value of Journey 01 (ít bạn) = 7787
		 // value of Journey 02 (hóng hớt) = 7796
		 // value of Journey 03 (thích tương tác) = 7806
		 // value of Journey 03 (thích giao lưu) = 7807
		WebElement save_setup_button = driver.findElement(By.xpath("//*[contains(text(), 'Lưu thiết lập')]"));
		save_setup_button.click();
		TimeUnit.SECONDS.sleep(4);
		WebElement checkbox_button_2 = driver.findElement(By.cssSelector("input.form-check-input-styled"));
		if(!checkbox_button_2.isSelected()){
			checkbox_button_2.click();
		}
//		WebElement run_button = driver.findElement(By.xpath("//*[contains(text(), 'Chạy tương tác')]"));
//		run_button.click();
		return true;
	}
	public static void SendEmailNotification() throws FileNotFoundException {
		System.setProperty("file.encoding","UTF-8");
		ChromeOptions ops = new ChromeOptions();
		ops.addArguments("--disable-notifications");
		System.setProperty("webdriver.chrome.driver", "chromedriver_win32.exe");
		WebDriver driver = new ChromeDriver(ops);
		driver.manage().window().maximize();
		driver.get("https://phone.phanmemninja.com/home");
		String login_email = "procurement@reputable.asia";
		String login_password = "0901486137"; //	procurement@reputable.asia - 0901486137
		WebElement email_input = driver.findElement(By.name("email"));
		email_input.sendKeys(login_email);
		WebElement password_input = driver.findElement(By.name("password"));
		password_input.sendKeys(login_password);
		WebElement login_button = driver.findElement(By.cssSelector("form.login-form button"));
		login_button.click();
		WebElement button = driver.findElement(By.xpath("//*[contains(text(), 'Xem')]"));
		button.click();		
		List<WebElement> listofuid = driver.findElements(By.cssSelector("table.table-striped tbody tr"));
		//	send mail
		final String fromEmail = "apps@elangaming.com"; //requires valid gmail id
		final String password = "ra983811"; // correct password for gmail id
		final String toEmail = "trump@digityze.asia"; // can be any email id 
		final String ccEmails = "trump@reputyze.asia";
		System.out.println("TLSEmail Start");
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
		props.put("mail.smtp.port", "587"); //TLS Port
		props.put("mail.smtp.auth", "true"); //enable authentication
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		//create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);
		// time
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		DateTimeFormatter current_time = DateTimeFormatter.ofPattern("HH:mm");
		LocalDateTime now = LocalDateTime.now();
		String message = "";
		String subject = "Đã chạy tương tác Ninja Phone!";
		String journey_name = "Journey 1 (ít bạn)";
		message = message.concat("UID tương tác:\n");
		int i = 0;
		String[] b = null;
		for(WebElement uid : listofuid) {
			b = Arrays.toString(uid.getText().split("\n")).split(",");
			Pattern pattern = Pattern.compile("Đăng nhập thành công", Pattern.CASE_INSENSITIVE);
		    Matcher matcher = pattern.matcher(b[3]);
		    boolean matchFound = matcher.find();
		    if(matchFound) {
		    	message = message.concat(b[1]+"\n");
		    	i++;
		    }			
		}
		int time = i * 10;
		int hours = time / 60; 
		int minutes = time % 60 ;
		System.out.println(hours + ":" + minutes);
		System.out.println(current_time.format(now));
		message = message.concat("Thời gian: " + dtf.format(now) + "\n");
		message = message.concat("Thời gian dự kiến: " + dtf.format(now) + "\n");
		message = message.concat("Kịch bản: " + journey_name + "\n");
		EmailUtil.sendEmail(session, toEmail, ccEmails, subject, message);
		driver.close();
	}
}